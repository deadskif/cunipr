#include <stdio.h>
#include "cunipr.h"

typedef struct wtf {
    int l;
    const char *n;
} wtf;
int fprint_wtf(FILE *f, const struct wtf *p) 
{
    return fprintf(f, "{l %u, n %s}", p->l, p->n);
}

typedef struct ololo {
    const char *w;
    int g;
} ololo;
int fprint_ololo(FILE *f, const struct ololo *p)
{
    return fprintf(f, "{%s:%i}", p->w, p->g);
}


#define UNIP(V) _Generic((V), \
            wtf *: UNIP_TYPE(wtf, (V)), \
            ololo *: UNIP_TYPE(ololo, (V)) \
        )

#define PRINT_TEST(PRI) do { \
    int __l; \
    fprintf(stderr, #PRI " " PRI ": "); \
    __l = fprintf(stderr, "%" PRI " %" PRI "\n", UNIP(&w), UNIP(&o)); \
    fprintf(stderr, "Len %d\n", __l); \
} while(0)
int main(int argc, const char *argv[])
{

    wtf w = {
        .l = 42,
        .n = "Vasya",
    };
    ololo o = {
        .w = "O_O",
        .g = -1,
    };

    PRINT_TEST(UNIP_MODIFIER_PRI);
    PRINT_TEST(UNIP_SPECIFIER_PRI);
    fprintf(stderr, "%p %p \n", &w, &o);
    return 0;
}
