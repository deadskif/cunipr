
SHELL	= /bin/sh
CC		?= gcc
AR 		?= ar
RM		?= rm 

INSTALL ?= install
INSTALL_PROGRAM = $(INSTALL)
INSTALL_DATA = ${INSTALL} -m 644

CFLAGS = -O2 -Wall -Wextra -fPIC


prefix = /usr/local
exec_prefix = $(prefix)

bindir = $(exec_prefix)/bin
libdir = $(exec_prefix)/lib

srcdir = .


.SUFFIXES:
.SUFFIXES: .c .o

.PHONY: all install clean

LIB = libcunipr.so

all: $(LIB) test 

$(LIB): cunipr.c cunipr.h $(MAKEFILES)
	$(CC) $(CFLAGS) $^ -shared -o $@

test: test.c $(LIB) $(MAKEFILES)
	$(CC) -Wl,-rpath,. -L. -lcunipr $^ -o $@ 

clean:
	$(RM) $(LIB) test
