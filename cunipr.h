#ifndef CUNIPR_H

#define CUNIPR_H
#include <stdio.h>

#define UNIP_MODIFIER
#define UNIP_SPECIFIER

typedef int (*cunipr_print)(FILE *f, const void *);
struct cunipr {
    int (*f)(FILE *f, const void *);
    const void *v;
};

#define UNIP_INIT(F, V) (struct cunipr){ \
    .f = (cunipr_print)(F), \
    .v = (V) \
}
#define UNIP_TYPE(T, V) UNIP_INIT(fprint_ ## T, V)

#ifdef UNIP_MODIFIER
# define UNIP_MODIFIER_PRI "Op"
#endif
#ifdef UNIP_SPECIFIER
# define UNIP_SPECIFIER_PRI "P"
#endif

#ifndef UNIP_PRI
# if defined(UNIP_MODIFIER)
#  define UNIP_PRI UNIP_MODIFIER_PRI
# elif defined(UNIP_SPECIFIER)
#  define UNIP_PRI "P"
# endif
#endif

#endif /* end of include guard: CUNIPR_H */

