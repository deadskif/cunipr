#include <stdio.h>
#include <string.h>
#include <printf.h>

#include "cunipr.h"

static int cunipr_type;
#ifdef UNIP_MODIFIER
static int cunipr_mod;
#endif

static void cunipr_va(void *mem, va_list *ap)
{
    struct cunipr u = va_arg(*ap, struct cunipr);
    memcpy(mem, &u, sizeof(u));
}
static int cunipr_arginfo (const struct printf_info *info,
        size_t n, int *argtypes, int *sizes)
{
    /* We always take exactly one argument and this is a pointer to the
     structure.. */
    if (n > 0) {
        argtypes[0] = cunipr_type;
        sizes[0] = sizeof(struct cunipr);
    }
    return 1;
}

#ifdef UNIP_MODIFIER
static int cunipr_printf_p_mod (FILE *stream, 
        const struct printf_info *info, 
        const void *const *args)
{
    if (info->user & cunipr_mod) {
        const struct cunipr *u = *(const struct cunipr **)args[0];
        return u->f(stream, u->v);
    } else
        return -2;
    return 0;
}
#endif
#ifdef UNIP_SPECIFIER
static int cunipr_printf_spec (FILE *stream,
        const struct printf_info *info,
        const void *const *args)
{
    const struct cunipr *u = *(const struct cunipr **)args[0];

    return u->f(stream, u->v);
}
#endif

__attribute__((constructor))
static int cunipr_initalize_printf_specifier(void)
{
    cunipr_type = register_printf_type(cunipr_va);
#ifdef UNIP_SPECIFIER
    register_printf_specifier(UNIP_SPECIFIER_PRI[0], cunipr_printf_spec, cunipr_arginfo);
#endif
#ifdef UNIP_MODIFIER
    cunipr_mod = register_printf_modifier(L"O");
    register_printf_specifier (UNIP_MODIFIER_PRI[1], cunipr_printf_p_mod, cunipr_arginfo);
#endif
    return 0;
}
